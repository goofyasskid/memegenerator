from core import generator
import os

def get_text(message):
    mytext = message.text
    return mytext


def data_checking(top, bottom, file):
    if file and top and bottom is not None:
        check = 'У вас все есть, можно приступать к созданию!'
    else:
        lst_check = []
        if file is None:
            lst_check.append('не хватает картинки' + '\n')
        if top is None:
            lst_check.append('нет верхней надписи' + '\n')
        if bottom is None:
            lst_check.append('нет нижней надписи' + '\n')
        check = ''.join(lst_check)
    return check


def launch_it(top, bottom, file, dir_adr):
    img = generator.make_meme(dir_adr, top, bottom, file)
    os.remove(str(dir_adr) +'/images/' + str(file) + '.jpg')
    return img
